# GIT Basics

Welcome to this course.

This is a course about day-to-day usage of GIT for software development.

Here you will find
* Installation instructions
* Solutions to the exercises

# Installation instructions
You need to have a GIT client installed to perform the exercises
and to clone this repo.

* [GIT Client Download](https://git-scm.com/downloads)

Choose the installer for your platform, e.g. Windows 64-bit.
You can go for most of the default settings. Ensure you
select to install BASH.
Do not enable the "Git Credential Manager" during the install,
because it might not work with gitlab.

You also need a simple text editor or IDE, such as any of
* Notepad++
* TextPad
* MS Visual Code


# GitLab Account
During the course you will be creating and interacting with a
remote git repo. We are using GitLab for this part of the course.
Sign up for a free account at GitLab:
* [Register for GitLab](https://gitlab.com/users/sign_up)


## Corporate Proxies
Sometimes, firewalls/proxies block outgoing SSH connections.
Verify that this is not the case or find out how to configure.
The following links might shed some light
* [Git Behind A Proxy Server](https://www.freecodecamp.org/forum/t/git-behind-a-proxy-server-how-to-modify-your-git-commands-for-proxies/13187)
* [Configure Git to use a proxy](https://gist.github.com/evantoli/f8c23a37eb3558ab8765)


# Cloning this Repo
You should clone this git repo, by the following command

    mkdir -p ~/git-course/my-solutions
    cd ~/git-course
    git clone https://gitlab.com/ribomation-courses/dev-tools/git-basics.git gitlab

After an update, by the teacher, perform the following command

    cd ~/git-course
    git pull

Links
====
* [GIT](https://git-scm.com/)
* [GitLab](https://about.gitlab.com/)
* [GitHub](https://github.com/)
* [Code Review Tools](https://en.wikipedia.org/wiki/List_of_tools_for_code_review)
* [How to Version Control Jupyter Notebooks](https://nextjournal.com/schmudde/how-to-version-control-jupyter)
* [Git Version Control with Jupyter Notebooks](https://towardsdatascience.com/version-control-with-jupyter-notebooks-f096f4d7035a)
* [Blocking large file commits in git](http://gregmac.net/git/2013/06/21/blocking-large-file-commits-in-git.html)
* [Why and How to Use Git LFS](https://dzone.com/articles/git-lfs-why-and-how-to-use)
* [GIT LFS - Large File Storage](https://github.com/git-lfs/git-lfs)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
