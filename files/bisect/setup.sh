#!/usr/bin/env bash
set -e

echo "** Resetting project"
rm -rf .git book.txt
git init -q
git config core.autocrlf false
git config alias.l 'log --oneline --decorate'

git add .gitignore
git commit -qm 'added ignore'
git l

echo "** Now, run ./mk-history.sh"

