#!/bin/bash

FILE=readme.txt

echo "RESOLVE conflict, by sed:ing that target file"
sed -i '/^<<<.*/d' $FILE 2>/dev/null
sed -i '/^===.*/d' $FILE 2>/dev/null
sed -i '/^>>>.*/d' $FILE 2>/dev/null

sed -i '5s/.*/resolved feature and master/' $FILE 2>/dev/null
sed -i '/^conflict.*/d' $FILE 2>/dev/null

git add $FILE 
git commit -m "finally fixed that pesky merge"

git log --oneline --graph --all --decorate

gitk --all&
