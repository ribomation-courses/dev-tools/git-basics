#!/bin/bash
set -e

FILE=readme.txt

echo "FEATURE updates"
set -x
git checkout master
git checkout -b feature
sed -i '1s/.*/this is a feature/' $FILE 2>/dev/null
git add $FILE
git commit -m "2nd commit (feature)"


echo "MASTER updates"
git checkout master
sed -i '10s/.*/this is another feature/' $FILE 2>/dev/null
git add $FILE
git commit -m "3rd commit (master)"


echo "MERGE, conflict-free"
git merge feature -m "successfully merged feature back into master"

gitk --all&
