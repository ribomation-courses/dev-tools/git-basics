#!/bin/bash
set -e

FILE=readme.txt

echo "Reset project folder"
rm -rf .git .gitignore *.txt
git init -q

echo '*.sh' > .gitignore
git add .gitignore
git commit -m 'ignore shell scripts'

echo "Generating content"
for n in {1..10}; do
  echo "line #$n" >> $FILE
done
git add $FILE
git commit -m "1st commit"
cat $FILE
