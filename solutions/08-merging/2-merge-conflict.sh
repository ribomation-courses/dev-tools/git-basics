#!/bin/bash

FILE=readme.txt

echo "FEATURE update"
git checkout feature
sed -i '5s/.*/conflict from feature/' $FILE 2>/dev/null
git add $FILE
git commit -m "commit #4 (feature)"

echo "MASTER update"
git checkout master
sed -i '5s/.*/conflict from master/' $FILE 2>/dev/null
git add $FILE
git commit -m "commit #5 (master)"


echo "MERGE, that will fail"
git merge feature -m 'trying to merge with conflict...'

echo "-+-+-+-+-+-+-+-+-+-"
cat $FILE
echo "-+-+-+-+-+-+-+-+-+-"

echo "fix the merge conflict and proceed by"
echo "    git add $FILE"
echo "    git commit -m 'finally fixed that merge hazzle'"

