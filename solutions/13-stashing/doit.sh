#!/usr/bin/env bash
set -x

FILE=file-txt
FILE2=another-file.txt

rm -rf .git *.txt
git init
git config core.autocrlf false
git config alias.l 'log --oneline --decorate --all --graph'

echo 'this is the 1st line' > $FILE
git add $FILE
git commit -m '1st commit'

echo 'this is the 2nd row' >> $FILE


git stash save 'saved stuff'
git stash list

echo 'this is the 3rd text' > $FILE2
git add $FILE2
git commit -m '2nd commit'
git l

git stash pop
git l
