#!/bin/bash
set -eu

# --- Prerequisites ---
# (1)  Ensure you have your email/username/password to gitlab
# (2)  Create a new project in gitlab, which must be empty

echo 'Enter Your GitLab info:'
read -p 'Email: ' EMAIL
read -p 'Username: ' USERNAME
read -p 'Reponame: ' PROJECT

URL=https://$USERNAME@gitlab.com/$PROJECT.git
REALNAME=`echo $EMAIL | sed 's/@.*$//'` 
REMOTE=gitlab
LOCAL=repo-migrated
FILE=silly-file.txt
FILE2=yet-another-file.txt
BRANCH=tjollahopp

GITHUB_URL=https://github.com/ribomation/DroidAtScreen1.git
GITHUB_ALIAS=github


echo "** Clone"
set -x
rm -rf $LOCAL
git clone $GITHUB_URL $LOCAL
cd $LOCAL

echo "** Setup"
git config core.autocrlf false
git config alias.l 'log --oneline --decorate --all --graph'
git config user.name "$REALNAME"
git config user.email $EMAIL
git remote -v

echo "** Rename origin"
git remote rename origin $GITHUB_ALIAS
git remote -v

echo "** Associate with new fresh remote repo"
git remote add $REMOTE $URL
git remote -v
cat .git/config
git --no-pager l HEAD~5..

echo "** Upload to new repo"
git push -u $REMOTE --all
git push -u $REMOTE --tags

echo "** Skip association with old repo"
git remote remove $GITHUB_ALIAS
cat .git/config

echo "** A new commit and push it"
echo 'tjabba habba babba' > another-file.txt
git add another-file.txt
git commit -m 'added another-file.txt'
git --no-pager l HEAD~5..

git push
git --no-pager l HEAD~5..

echo "** Create a new tracked branch"
git checkout -b $BRANCH --track

echo 'hello remote git repo' > $FILE2
git add $FILE2
git commit -m "added $FILE2"

echo 'this is a friendly greeting' >> $FILE2
git add $FILE2
git commit -m "updated $FILE2"

git --no-pager l HEAD~5..

git push $REMOTE $BRANCH
git l HEAD~5..

git checkout master
git merge $BRANCH
git --no-pager l HEAD~5..

git push
git --no-pager l HEAD~5..

