#!/bin/bash
set -e
#set -x

FILE=myfile.txt

echo "-- RESET DIR ---"
rm -rf .git $FILE

echo "-- GIT Init ---"
git init

echo "-- Adding content... ---"
for n in {1..10}; do
  echo "line #$n" >> $FILE
  git add $FILE
  git commit -m "commit #$n"
done

echo "-- Commits---"
git log --oneline --decorate
