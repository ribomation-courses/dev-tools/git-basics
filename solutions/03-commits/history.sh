#!/bin/bash
set -e
#set -x

FILE=myfile.txt

echo "-- History evolution ---"
for ((n=9; n>=0; n-=1)); do 
  echo "-- $n --"
  git show HEAD~$n:$FILE
done

