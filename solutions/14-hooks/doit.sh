#!/bin/bash

rm -rf .git file.txt
git init -q
git config core.autocrlf false

cat <<"EOT" > .git/hooks/commit-msg
#!/bin/bash

N=`cat $1 | wc -c | tr -d ' '`
if (( $N < 10 )); then
  echo "no, no: Yo man, need to be more chatty ;-)"
  exit 1
fi
EOT
chmod +x .git/hooks/commit-msg

echo 'testing a tiny hook' > file.txt
(set -x; git add file.txt)

echo ""
echo '*** too short commit msg -> fail'
(set -x; git commit -m 'abc')

echo ""
echo '*** long commit msg -> success'
(set -x; git commit -m 'yada, yada. bla, bla')

echo ""
(set -x; git log --oneline --decorate)
