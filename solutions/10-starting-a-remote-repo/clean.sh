#!/bin/bash
set -eux

rm -rf repo-*

echo "N.B. You still need to manually delete the remote repos!"
