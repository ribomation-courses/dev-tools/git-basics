#!/bin/bash
set -eu

# --- Prerequisites ---
# (1)  Ensure you have your email/username/password to gitlab
# (2)  Create a new project in gitlab, which must be empty
# (3)  Do not commit to the new project; leave it empty

echo 'Enter Your GitLab info:'
read -p 'Email: ' EMAIL
read -p 'Username: ' USERNAME
read -p 'Reponame: ' PROJECT

URL=https://$USERNAME@gitlab.com/$PROJECT.git
REALNAME=`echo $EMAIL | sed 's/@.*$//'` 
REMOTE=gitlab
LOCAL=repo-uploaded
FILE=silly-file.md


set -x
rm -rf $LOCAL
mkdir -p $LOCAL 
cd $LOCAL

git init
git config user.name "$REALNAME"
git config user.email "$EMAIL"
git config alias.l 'log --oneline --decorate --all --graph'
git remote add $REMOTE $URL
git remote -v
cat .git/config

cat <<EOT > README.md
Project GIT Upload
====

Yet another header
------
Some random text here as well
EOT

git add README.md
git commit -m 'initial import'
git --no-pager l
git push --set-upstream $REMOTE master
cat .git/config
git --no-pager l


cat <<EOT > $FILE
This is just a 
plain text file
with some lines in it.
EOT

git add .
git commit -m 'added a new file'
git status
git l

echo '' >> $FILE
echo 'Another paragraph...' >> $FILE
git commit -m 'more lines added' $FILE

git status
git l
git push
git status
git l

