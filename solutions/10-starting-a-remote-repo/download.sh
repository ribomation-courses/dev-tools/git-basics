#!/bin/bash
set -eu

# --- Prerequisites ---
# (1)  Ensure you have your email/username/password to gitlab
# (2)  Create a new project in gitlab
# (3)  Add at least one commit to the project

echo 'Enter Your GitLab info:'
read -p 'Email: ' EMAIL
read -p 'Username: ' USERNAME
read -p 'Reponame: ' PROJECT

URL=https://$USERNAME@gitlab.com/$PROJECT.git
REALNAME=`echo $EMAIL | sed 's/@.*$//'`
REMOTE=gitlab
LOCAL=repo-downloaded
FILE=some-file.md


set -x
rm -rf $LOCAL
git clone $URL $LOCAL
cd $LOCAL
git config user.name "$REALNAME"
git config user.email "$EMAIL"
git config alias.l 'log --oneline --decorate --all --graph'
git remote rename origin $REMOTE
cat .git/config
git l

cat <<EOT >> $FILE
some-file
====

Yet another header
------
Some random text here as well
EOT
git add $FILE
git commit -m '1st local commit'

git status
git l
git push
git status
git l

