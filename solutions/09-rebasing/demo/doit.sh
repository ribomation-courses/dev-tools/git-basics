#!/bin/bash
set -e

BASE=file
EXT=txt
BRANCH=feature

function addChange {
  N=$1
  F="$BASE-$N.$EXT"
  echo "line no. $N" >> $F
  git add $F
  git commit -m "commit no. $N"
  git l
}

rm -rf .git .gitignore *.txt
git init
git config alias.l 'log --oneline --decorate --all --graph'

echo '*.sh' > .gitignore
git add .gitignore
git commit -m 'ignore shell-script'

addChange 1
git branch $BRANCH
addChange 21
addChange 22

git checkout $BRANCH
addChange 31
addChange 32

git rebase master
git l

git checkout master
git merge --ff-only $BRANCH
git l
git status
