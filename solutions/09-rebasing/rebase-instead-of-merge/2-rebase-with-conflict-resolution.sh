#!/bin/bash
#set -e
set -x

FILE=readme.txt
BRANCH=feature


echo "*** updating line 5 in branch $BRANCH"
git checkout $BRANCH
sed -i '5s/.*/conflict from feature/' $FILE 2>/dev/null
git add $FILE
git commit -m "commit #4 ($BRANCH)"
git l

echo "*** updating line 5 in branch master"
git checkout master
sed -i '5s/.*/conflict from master/' $FILE 2>/dev/null
git add $FILE
git commit -m "commit #5 (master)"
git l

echo '*** starting a rebase with conflict'
git checkout $BRANCH
git rebase master
git l
git status
cat $FILE

echo '*** resolving the conflict'
sed -i '/^<<<.*/d' $FILE 2>/dev/null
sed -i '/^===.*/d' $FILE 2>/dev/null
sed -i '/^>>>.*/d' $FILE 2>/dev/null
sed -i '/^conflict.*feature/d' $FILE 2>/dev/null
sed -i '5s/.*/resolved feature and master/' $FILE 2>/dev/null
cat $FILE

echo '*** proceeding with the rebase'
git add $FILE
git rebase --continue
git l

echo '*** aligning the branches'
git checkout master
git merge $BRANCH --ff-only
git l

