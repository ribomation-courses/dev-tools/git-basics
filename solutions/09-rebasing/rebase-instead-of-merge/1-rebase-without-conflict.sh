#!/bin/bash
set -e
set -x

FILE=readme.txt
BRANCH=feature

echo '*** project reset'
rm -rf .git .gitignore $FILE
git init
git config core.autocrlf false
git config alias.l 'log --oneline --decorate --all --graph'

echo '*.sh' > .gitignore
git add .gitignore
git commit -m 'ignore shell scripts'

set +x
echo '*** creating file with 10 lines'
for n in {1..10}; do
  echo "line #$n" >> $FILE
done
set -x
git add $FILE
git commit -m "1st commit"


echo "*** updating the first line in branch $BRANCH"
git checkout -b $BRANCH
sed -i '1s/.*/this is a feature/' $FILE 2>/dev/null
git add $FILE
git commit -m "2nd commit (feature)"


echo "*** updating the first line in branch master"
git checkout master
sed -i '10s/.*/this is another feature/' $FILE 2>/dev/null
git add $FILE
git commit -m "3rd commit (master)"


echo '*** performing the rebase'
git l
git checkout $BRANCH
git rebase master 
git l
git checkout master
git merge $BRANCH --ff-only
git l

