#!/usr/bin/env bash
set -x

ARCHIVE=my-project.tar.gz

git archive --format=tar HEAD | gzip > $ARCHIVE
tar tvfz $ARCHIVE
