#!/usr/bin/env bash
set -u

FILE=readme.txt
FILE2=sample.txt
FILE3=whatever.txt
BRANCH=feature

echo "** Setup"
rm -rf .git $FILE
git init -q
git config core.autocrlf false
git config alias.l 'log --oneline --decorate --all --graph'

echo "** First commit"
echo 'this is the 1st line' > $FILE
echo 'tjabba habba' > $FILE2
echo 'foobar strikes again' > $FILE3
git add $FILE $FILE2 $FILE3
git commit -qm '1st commit'
git l

echo "** Creating $BRANCH"
git branch $BRANCH

echo '*** still on master'
git branch -v
echo 'the second line' >> $FILE
git add $FILE
git commit -qm '2nd commit'
git l
echo  "*** Ooops, commited to the wrong branch"

COMMIT_ID=` git log --oneline HEAD^.. | cut -d ' ' -f 1 | tr -d '\n'`
echo "** Got the commit id=$COMMIT_ID"

echo "** Going back in time"
git reset --hard HEAD^
git l

echo "** Switching to $BRANCH"
git checkout $BRANCH
git branch -v

echo "** Applying the edits in the correct branch"
git cherry-pick $COMMIT_ID
git l

