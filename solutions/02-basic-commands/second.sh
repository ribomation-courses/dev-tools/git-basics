#!/bin/bash
set -x

git clone https://github.com/ribomation/DroidAtScreen1.git
cd DroidAtScreen1
ls -lhFA
git --no-pager log --oneline --decorate --graph --all

shopt -s extglob
rm -rf -- !(.git) 2>/dev/null
ls -lhFA

git reset --hard
ls -lhFA
