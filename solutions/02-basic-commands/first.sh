#!/bin/bash
set -x

mkdir simple && cd $_
git init

echo 'just some silly text' > readme.txt
git status
git add readme.txt
git status
git log
git commit -m 'my 1st commit'
git log

echo 'the second line' >> readme.txt
git status
git add readme.txt
git commit -m 'my 2nd commit'
git log
git log --oneline

git show
git show HEAD
git show HEAD^
