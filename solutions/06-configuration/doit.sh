#!/bin/bash
set -eu

FILE=the-file.txt

echo "** Resetting the project folder"
rm -rf .git .gitignore $FILE
git init -q

echo "** Setting aliases"
set -x
git config alias.s status
git config alias.l 'log --oneline --decorate --all --graph'
cat .git/config

set +x
echo "** Using alias 's'; will includes this shell script"
git s

echo "** Ignoring all shell scripts"
echo '*.sh' > .gitignore
set -x
git s
git add .gitignore
git s
git commit -qm 'added ignore rules'
git s
git l

set +x
echo "** Performing a commit, so we can use alias 'l'"
echo 'tjabba habba' >> $FILE
set -x
git s
git add $FILE
git commit -qm '1st commit'
git l
