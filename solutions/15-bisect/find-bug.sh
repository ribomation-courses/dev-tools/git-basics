#!/usr/bin/env bash
set -eu

UNIT_TEST=unit-test.sh

echo "** Creating test script"
cat <<EOT > $UNIT_TEST
#!/usr/bin/env bash
grep -q BUG book.txt && exit 1
exit 0
EOT

echo "** Starting the bisect operation..."
FIRST_COMMIT=`git log --oneline | tail -1 | cut -d ' ' -f 1 | tr -d '\n'`
echo "** Start commit is $FIRST_COMMIT"

set -x
git bisect start HEAD $FIRST_COMMIT --
git bisect run ./$UNIT_TEST
set +x

echo ''
echo "** Done"
BAD_COMMIT=`git bisect view --stat --oneline | head -1`
git bisect reset

echo ''
echo "*** Found the first bad commit: $BAD_COMMIT"
