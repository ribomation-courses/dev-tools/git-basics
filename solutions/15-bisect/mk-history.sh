#!/usr/bin/env bash
set -eu

NUM_COMMITS=100
LB=10
UB=$[$NUM_COMMITS - 10]
BITE=30
SRC=source.txt
DST=book.txt
BUGIDX=`shuf -i $LB-$UB -n 1`

if [ ! -r "$SRC" ]
then
    echo "Missing source file: $SRC"
    exit 1
fi

rm -f $DST
for k in `seq 1 $NUM_COMMITS`
do
    POS=$[$k * $BITE + 1]
    echo "Chunk $k: lines $POS..$[$POS + $BITE]" 

    tail -n +$POS $SRC | head -$BITE >> $DST
    if [ $BUGIDX -eq $k ]; then
        echo '---BUG---' >> $DST
    fi

    git add $DST
    git commit -m "commit number $k"
done

echo ''
echo "** Create file 'book.txt' and a bunch of commits"
echo "** One commit, introduced the bug. Find it!"
