#!/usr/bin/env bash
set -e

echo "** Resetting the project"
rm -rf .git book.txt unit-test.sh
git init -q
git config core.autocrlf false
git config alias.l 'log --oneline --decorate'

echo '*.sh' > .gitignore
echo 'source.txt' >> .gitignore
git add .gitignore
git commit -qm 'added ignore'
git l

echo "** Now, run ./mk-history.sh"

