#!/bin/bash
set -e

FILE=fancy-stuff.txt

echo "-- Reset folder"
rm -rf .git $FILE

echo "-- GIT Init"
git init -q

echo "-- Adding content..."
for n in {1..8}; do
  echo "feature #$n" >> $FILE
  git add $FILE
  git commit -qm "feat(core): commit #$n"

  echo "oh crap, bug #$n" >> $FILE
  git add $FILE
  git commit -qm "fix(core): fixed that pesky bug #$n"
done

echo "-- List of Features --"
git log --oneline --grep '^feat'

echo "-- List of Bugfixes --"
git log --oneline --grep '^fix'

