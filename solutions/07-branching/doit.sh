#!/bin/bash
set -e

FILE=readme.txt
FILE2=another-file.txt
FILE3=yet-another-file.txt
BRANCH1=first
BRANCH2=second
BRANCH3=tjollahopp
BRANCH4=whatever

echo '--- Reset project folder ---'
rm -rf .git $FILE $FILE2 $FILE3
git init -q


echo '--- Some initial commits ---'
echo "first line, in master" >> $FILE
set -x
git add $FILE
git commit -qm "1st commit"
echo "2nd line, in master" >> $FILE
git add $FILE
git commit -qm "2nd commit"


echo '--- FIRST branch, with some commits ---'
git branch $BRANCH1
git checkout $BRANCH1
echo "one line in branch '$BRANCH1'" >> $FILE
git add $FILE
git commit -qm "my commit in $BRANCH1"
echo "another line in branch '$BRANCH1'" >> $FILE
git add $FILE
git commit -qm "my 2nd commit in branch $BRANCH1"


echo '--- SECOND branch, also with some commits ---'
git checkout master
git checkout -b $BRANCH2
echo "line in branch '$BRANCH2'" >> $FILE
git add $FILE
git commit -qm "3rd commit, now in $BRANCH2"


echo '--- Switching back again to master and listing all branches ---'
git checkout master
git branch --list


echo '--- Renaming the 2nd branch ---'
git branch --move $BRANCH2 $BRANCH3
git branch
git --no-pager log --oneline --decorate --all --graph


echo '--- Creating a branch from a detached head ---'
git checkout $BRANCH1
git checkout HEAD^
git checkout -b $BRANCH4
touch $FILE2
git add $FILE2
git commit -m "a commit in branch $BRANCH4"
git --no-pager log --oneline --decorate --all --graph

echo '--- Back again to master ---'
git checkout master
touch $FILE3
git add $FILE3
git commit -m 'last commit for this time, now in master'

gitk --all&

