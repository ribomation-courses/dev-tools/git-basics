#!/bin/bash
set -x

echo '*** adding two tags'
git tag -am 'the grand-parent of the latest' granny HEAD^^
git tag -am 'the first commit' first HEAD~9
git tag -n
git log --oneline --decorate
git --no-pager show granny

echo '*** removing the tags again'
git tag -d granny
git tag -n
git log --oneline --decorate
git tag -d first
git tag -n
git log --oneline --decorate
